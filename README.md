# 1.20.4 NeoForge -> 1.20.5 NeoForge

## Eclipse
- Install `Record Patterns and Pattern Switches in Eclipse 2023-09 (4.29)` for Java 21 support
	- Eclipse itself may need to be updated to do this.

## General Changes
- Update `gradle/wrapper` folder to use gradle 8.6 (just grab the files from this project)

## `gradle.properties` Changes
- Minecraft version: `1.20.5`
- Minecraft version range: `[1.20.5,1.21)`
- NeoForge: `20.5.3-beta`
- NeoGradle: `7.0.105`
- Mappings: `1.20.5`
- Parchment: `1.20.4` and `2024.04.14`

## `build.gradle` Changes
- Changed `write_paths` field in toml generation from `mods.toml` to `neoforge.mods.toml`
- Changed `java.toolchain.languageVersion = JavaLanguageVersion.of(17)` to `java.toolchain.languageVersion = JavaLanguageVersion.of(21)`

## Tags
- `forge` namespace tags should be replaced with `c` namespace as that's the new convention.

## Item Data Components
- Need to be upgraded between worlds to handle legacy NBT.

## Networking
- Rewrote networking... again... to be Codec driven
- _yaaaaay_

## Misc
- `@Mod.EventBusSubscriber` -> `@EventBusSubscriber`
- The `FORGE` Bus has been renamed to `GAME`
- Your mod class constructor now needs to be `public ExampleMod(IEventBus modBus, Dist dist, ModContainer container)`
	- `ModContainer` is how you register your configs now

# 1.20.1 MinecraftForge -> 1.20.2 NeoForge

## Remapping script
Run this before updating to 1.20.2. Some classes won't get remapped right, known ones are listed further down. This will also import `var` and `record` all over the place. You'll need to organize imports once you're done with everything else.
https://gist.github.com/Technici4n/facbcdf18ce1a556b76e6027180c32ce

## Before Importing
- Copy the following folders:
	- `gradle`
	- `workspace_gen` (for the mods.toml generator jar)
- Copy the following files:
	- `.gitignore` (It needs to account for workspace_gen jars now)
	- `build.gradle`
	- `gradle.properties`
	- `settings.gradle`
	- `mods.toml` (from the root directory. This is a template file)
- Delete the following files:
	- `.classpath` It may cause issues and needs to be regenerated anyway.
- Access transformers use mapped names now.
- `ObfuscationReflectionHelper` uses mapped names now.

## Known manual remaps because the remapping script above won't do it right
- `NeoForgeConfigSpec` -> `ModConfigSpec`
- `NetworkDirection` -> `PlayNetworkDirection`

## PacketHandler/NetworkHandler
- registerMethod
```private static <MSG> void register(int index, Class<MSG> packet, MessageFunctions.MessageEncoder<MSG> encoder, MessageFunctions.MessageDecoder<MSG> decoder,  MessageFunctions.MessageConsumer<MSG> messageConsumer)```
- The `handle` method in a packet now takes `NetworkEvent.Context` instead of `Supplier<NetworkEvent.Context>` (find and replace works wonders)

## Events
- `event.isCancellable()` no longer exists. Use `event instanceof ICancellableEvent`

## Gui Stuff
- `GuiEventListener.mouseScrolled()` has a 4th double argument, `offset`
- `EditBox.tick()` no longer exists. May not be needed.
- `Screen.render()` no longer should call `renderGrayBackground()`. Moving `super.render()` to it's place works most of the time.
- `Screen.renderBackground()` takes `GuiGraphics graphics, int mouseX, int mouseY, float partialTicks`

## Running the project
- Right click the project -> `Run as` -> `Run Configurations...` -> Select from the `Launch Group` section
- You can also get to `Run Configurations...` from the `Run` or `Debug` buttons.