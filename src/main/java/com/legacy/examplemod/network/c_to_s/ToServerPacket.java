package com.legacy.examplemod.network.c_to_s;

import com.legacy.examplemod.ExampleMod;

import net.minecraft.network.RegistryFriendlyByteBuf;
import net.minecraft.network.codec.StreamCodec;
import net.minecraft.network.protocol.common.custom.CustomPacketPayload;
import net.neoforged.neoforge.network.handling.IPayloadContext;

// Triggered when a player left clicks diamond ore on the client
public record ToServerPacket(int yLevel) implements CustomPacketPayload
{
	public static final Type<ToServerPacket> TYPE = new Type<>(ExampleMod.locate("to_server"));

	public static final StreamCodec<RegistryFriendlyByteBuf, ToServerPacket> STREAM_CODEC = new StreamCodec<RegistryFriendlyByteBuf, ToServerPacket>()
	{
		@Override
		public ToServerPacket decode(RegistryFriendlyByteBuf buff)
		{
			return new ToServerPacket(buff.readInt());
		}

		@Override
		public void encode(RegistryFriendlyByteBuf buff, ToServerPacket packet)
		{
			buff.writeInt(packet.yLevel);
		}
	};

	@Override
	public Type<? extends CustomPacketPayload> type()
	{
		return TYPE;
	}

	public static void handle(ToServerPacket packet, IPayloadContext context)
	{
		ExampleMod.LOGGER.info("Recieved packet from client {} y={}", context.player(), packet.yLevel);
	}
}