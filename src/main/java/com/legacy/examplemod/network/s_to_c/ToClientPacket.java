package com.legacy.examplemod.network.s_to_c;

import com.legacy.examplemod.ExampleMod;

import net.minecraft.network.RegistryFriendlyByteBuf;
import net.minecraft.network.codec.StreamCodec;
import net.minecraft.network.protocol.common.custom.CustomPacketPayload;
import net.neoforged.neoforge.network.handling.IPayloadContext;

public record ToClientPacket(int yLevel) implements CustomPacketPayload
{
	public static final Type<ToClientPacket> TYPE = new Type<>(ExampleMod.locate("to_client"));

	public static final StreamCodec<RegistryFriendlyByteBuf, ToClientPacket> STREAM_CODEC = new StreamCodec<RegistryFriendlyByteBuf, ToClientPacket>()
	{
		@Override
		public ToClientPacket decode(RegistryFriendlyByteBuf buff)
		{
			return new ToClientPacket(buff.readInt());
		}

		@Override
		public void encode(RegistryFriendlyByteBuf buff, ToClientPacket packet)
		{
			buff.writeInt(packet.yLevel);
		}
	};

	@Override
	public Type<? extends CustomPacketPayload> type()
	{
		return TYPE;
	}

	public static void handle(ToClientPacket packet, IPayloadContext context)
	{
		ExampleMod.LOGGER.info("Recieved packet from server {} y={}", context.player(), packet.yLevel);
	}
}
