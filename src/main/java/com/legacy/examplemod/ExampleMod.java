package com.legacy.examplemod;

import org.slf4j.Logger;

import com.mojang.logging.LogUtils;

import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.fml.ModContainer;
import net.neoforged.fml.common.Mod;
import net.neoforged.fml.config.ModConfig;
import net.neoforged.neoforge.common.NeoForge;

@Mod(ExampleMod.MODID)
public class ExampleMod
{
	public static final String MODID = "examplemod";
	public static final Logger LOGGER = LogUtils.getLogger();

	public ExampleMod(IEventBus modBus, Dist dist, ModContainer container)
	{
		container.registerConfig(ModConfig.Type.COMMON, ExampleConfig.COMMON_SPEC);
		IEventBus forgeBus = NeoForge.EVENT_BUS;

		if (dist == Dist.CLIENT)
		{
			container.registerConfig(ModConfig.Type.CLIENT, ExampleConfig.CLIENT_SPEC);
		}

	}

	public static ResourceLocation locate(String key)
	{
		return new ResourceLocation(MODID, key);
	}

	public static final boolean IS_IDE = isRunningFromIDE();

	private static boolean isRunningFromIDE()
	{
		String p = System.getProperty(String.format("%s.iside", MODID));
		return Boolean.parseBoolean(p);
	}
}
