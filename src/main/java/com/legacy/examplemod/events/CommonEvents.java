package com.legacy.examplemod.events;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import com.legacy.examplemod.ExampleMod;
import com.legacy.examplemod.network.PacketHandler;
import com.legacy.examplemod.network.s_to_c.ToClientPacket;

import net.minecraft.DetectedVersion;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DataProvider;
import net.minecraft.data.PackOutput;
import net.minecraft.data.metadata.PackMetadataGenerator;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.server.packs.PackType;
import net.minecraft.server.packs.metadata.pack.PackMetadataSection;
import net.minecraft.util.InclusiveRange;
import net.minecraft.world.item.CreativeModeTab.TabVisibility;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Blocks;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.fml.event.lifecycle.FMLCommonSetupEvent;
import net.neoforged.neoforge.common.data.ExistingFileHelper;
import net.neoforged.neoforge.common.util.MutableHashedLinkedMap;
import net.neoforged.neoforge.data.event.GatherDataEvent;
import net.neoforged.neoforge.event.BuildCreativeModeTabContentsEvent;
import net.neoforged.neoforge.event.entity.player.PlayerInteractEvent;
import net.neoforged.neoforge.registries.RegisterEvent;

public class CommonEvents
{
	@EventBusSubscriber(bus = EventBusSubscriber.Bus.MOD)
	public static class ModEvents
	{
		@SubscribeEvent
		protected static void commonInit(FMLCommonSetupEvent event)
		{

		}

		@SubscribeEvent
		protected static void registerEvent(RegisterEvent event)
		{
			ResourceKey<?> registry = event.getRegistryKey();

			if (registry.equals(Registries.BLOCK))
			{

			}
			else if (registry.equals(Registries.ITEM))
			{

			}
			else if (registry.equals(Registries.ENTITY_TYPE))
			{

			}
			else if (registry.equals(Registries.BLOCK_ENTITY_TYPE))
			{

			}
		}

		@SubscribeEvent
		protected static void onTabLoad(final BuildCreativeModeTabContentsEvent event)
		{

		}

		private static void insertAfter(MutableHashedLinkedMap<ItemStack, TabVisibility> entries, List<ItemStack> items, ItemLike target)
		{
			ItemStack currentStack = null;
			for (var e : entries)
			{
				if (e.getKey().getItem() == target)
				{
					currentStack = e.getKey();
					break;
				}
			}

			for (var item : items)
				entries.putAfter(currentStack, currentStack = item, TabVisibility.PARENT_AND_SEARCH_TABS);
		}

		@SubscribeEvent
		protected static void gatherData(final GatherDataEvent event)
		{
			DataGenerator dataGen = event.getGenerator();
			ExistingFileHelper existingHelper = event.getExistingFileHelper();
			PackOutput packOutput = dataGen.getPackOutput();
			boolean server = event.includeServer();

			// Structure Gel stuff
			//RegistrarDatapackEntriesProvider registrarProv = ModList.get().isLoaded("test") ? RegistrarHandler.createGenerator(packOutput, StructureGelMod.MODID, "test") : RegistrarHandler.createGenerator(packOutput, StructureGelMod.MODID);
			//registrarProv = dataGen.addProvider(server, registrarProv);

			dataGen.addProvider(server, packMcmeta(packOutput, ExampleMod.MODID + " resources"));
		}

		private static final DataProvider packMcmeta(PackOutput output, String description)
		{
			int serverVersion = DetectedVersion.BUILT_IN.getPackVersion(PackType.SERVER_DATA);
			record NestedDataProvider<D extends DataProvider>(D provider, String namePrefix) implements DataProvider
			{
				@Override
				public CompletableFuture<?> run(CachedOutput cachedOutput)
				{
					return this.provider.run(cachedOutput);
				}

				@Override
				public String getName()
				{
					return this.namePrefix + "/" + this.provider.getName();
				}
			}
			return new NestedDataProvider<PackMetadataGenerator>(new PackMetadataGenerator(output).add(PackMetadataSection.TYPE, new PackMetadataSection(Component.literal(description), serverVersion, Optional.of(new InclusiveRange<>(0, Integer.MAX_VALUE)))), description);
		}
	}

	@EventBusSubscriber(bus = EventBusSubscriber.Bus.GAME)
	public static class ForgeEvents
	{
		@SubscribeEvent
		protected static void toClientPacketTest(PlayerInteractEvent.LeftClickBlock event)
		{
			if (event.getEntity() instanceof ServerPlayer player)
			{
				if (event.getLevel().getBlockState(event.getPos()).is(Blocks.GOLD_ORE))
				{
					PacketHandler.sendToClient(new ToClientPacket(event.getPos().getY()), player);
				}
			}
		}
	}
}
