package com.legacy.examplemod.events;

import com.legacy.examplemod.network.PacketHandler;
import com.legacy.examplemod.network.c_to_s.ToServerPacket;

import net.minecraft.world.level.block.Blocks;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.fml.event.lifecycle.FMLClientSetupEvent;
import net.neoforged.neoforge.event.entity.player.PlayerInteractEvent;

public class ClientEvents
{
	@EventBusSubscriber(bus = EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
	public static class ModEvents
	{
		@SubscribeEvent
		protected static void clientInit(FMLClientSetupEvent event)
		{

		}
	}

	@EventBusSubscriber(bus = EventBusSubscriber.Bus.GAME, value = Dist.CLIENT)
	public static class ForgeEvents
	{
		@SubscribeEvent
		protected static void toClientPacketTest(PlayerInteractEvent.LeftClickBlock event)
		{
			if (event.getLevel().isClientSide())
			{
				if (event.getLevel().getBlockState(event.getPos()).is(Blocks.DIAMOND_ORE))
				{
					PacketHandler.sendToServer(new ToServerPacket(event.getPos().getY()));
				}
			}
		}
	}
}
