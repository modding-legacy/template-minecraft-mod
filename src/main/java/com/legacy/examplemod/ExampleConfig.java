package com.legacy.examplemod;

import org.apache.commons.lang3.tuple.Pair;

import net.neoforged.neoforge.common.ModConfigSpec;

public class ExampleConfig
{
	public static final Common COMMON;
	public static final Client CLIENT;
	protected static final ModConfigSpec COMMON_SPEC;
	protected static final ModConfigSpec CLIENT_SPEC;

	static
	{
		Pair<Common, ModConfigSpec> specPairCommon = new ModConfigSpec.Builder().configure(Common::new);
		COMMON_SPEC = specPairCommon.getRight();
		COMMON = specPairCommon.getLeft();

		Pair<Client, ModConfigSpec> specPairClient = new ModConfigSpec.Builder().configure(Client::new);
		CLIENT_SPEC = specPairClient.getRight();
		CLIENT = specPairClient.getLeft();
	}

	public static class Common
	{
		public Common(ModConfigSpec.Builder builder)
		{

		}
	}

	public static class Client
	{
		public Client(ModConfigSpec.Builder builder)
		{

		}
	}
}
